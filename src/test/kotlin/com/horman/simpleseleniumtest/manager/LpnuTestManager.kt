package com.horman.simpleseleniumtest.manager

import com.horman.simpleseleniumtest.pages.lpnu.LpnuLoginPage
import com.horman.simpleseleniumtest.pages.lpnu.LpnuMainPage
import com.horman.simpleseleniumtest.pages.lpnu.LpnuModifyProfilePage
import com.horman.simpleseleniumtest.pages.lpnu.LpnuProfilePage
import org.junit.jupiter.api.Assertions
import org.openqa.selenium.WebDriver
import java.io.File

class LpnuTestManager(webDriver: WebDriver) {
    private val loginPage = LpnuLoginPage(webDriver)
    private val mainPage = LpnuMainPage(webDriver)
    private val profilePage = LpnuProfilePage(webDriver)
    private val modifyProfilePage = LpnuModifyProfilePage(webDriver)

    fun login(username: String, password: String) {
        Assertions.assertTrue(loginPage.isLoaded())
        Assertions.assertTrue(loginPage.enterUsername(username))
        Assertions.assertTrue(loginPage.enterPassword(password))
        Assertions.assertTrue(loginPage.pushLoginButton())
        Assertions.assertTrue(mainPage.isLoaded())
    }

    fun goToProfilePage() {
        Assertions.assertTrue(mainPage.isLoaded())
        Assertions.assertTrue(mainPage.clickActionMenu())
        Assertions.assertTrue(mainPage.clickDropdownProfileLink())
        Assertions.assertTrue(profilePage.isLoaded())
    }

    fun goToModifyProfilePage() {
        Assertions.assertTrue(profilePage.isLoaded())
        Assertions.assertTrue(profilePage.clickOptionsButton())
        Assertions.assertTrue(profilePage.clickDropdownMenuOption())
        Assertions.assertTrue(modifyProfilePage.isLoaded())
    }

    fun uploadPhoto(filePath: String) {
        Assertions.assertTrue(modifyProfilePage.isLoaded())
        val file = File(filePath)

        if (modifyProfilePage.isUserPictureSet()) {
            if (!modifyProfilePage.isDeletePictureInputSelected()) {
                Assertions.assertTrue(modifyProfilePage.toggleDeletePictureInput())
            }

            submitChangeAndValidate()
            goToProfilePage()
            goToModifyProfilePage()
        }

        Assertions.assertTrue(modifyProfilePage.clickUploadArrow())
        Assertions.assertTrue(modifyProfilePage.chooseFile(file.absolutePath))
        Assertions.assertTrue(modifyProfilePage.uploadFile())
        Assertions.assertTrue(modifyProfilePage.isNewImageAdded())
        submitChangeAndValidate()
    }

    fun submitChangeAndValidate() {
        Assertions.assertTrue(modifyProfilePage.clickSubmitButton())
        Assertions.assertTrue(mainPage.isLoaded())
        Assertions.assertTrue(mainPage.isSubmittedChangeSuccessful())
    }
}