package com.horman.simpleseleniumtest.manager

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.ExpectedCondition
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.WebDriverWait
import java.time.Duration


object WebDriverManager {
    private const val DEFAULT_TIMEOUT_IN_SEC = 7L
    lateinit var webDriver: WebDriver

    fun WebDriver.click(element: WebElement): Boolean {
        return doActionOnCondition(isClickable(element)) { element.click() }
    }

    fun WebDriver.sendKeys(element: WebElement, data: String): Boolean {
        return doActionOnCondition(isVisible(element)) { element.sendKeys(data) }
    }

    fun WebDriver.isClickable(element: WebElement): Boolean {
        return waitUntil(ExpectedConditions.elementToBeClickable(element))
    }

    fun WebDriver.isVisible(element: WebElement): Boolean {
        return waitUntil(ExpectedConditions.visibilityOf(element))
    }

    private fun WebDriver.doActionOnCondition(condition: Boolean, action: () -> Unit): Boolean {
      return waitUntil {
            if (condition) {
                action()
            }
            return@waitUntil true
        }
    }

    private fun <T> WebDriver.waitUntil(condition: ExpectedCondition<T>): Boolean {
        val wait = createWait(this)
        return try {
            wait.until(condition)
            true
        } catch (e: Exception) {
            false
        }
    }

    private fun createWait(webDriver: WebDriver): WebDriverWait {
        return WebDriverWait(webDriver, Duration.ofSeconds(DEFAULT_TIMEOUT_IN_SEC))
    }
}

