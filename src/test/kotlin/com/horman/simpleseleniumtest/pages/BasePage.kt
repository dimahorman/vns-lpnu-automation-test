package com.horman.simpleseleniumtest.pages

import org.openqa.selenium.WebDriver
import org.openqa.selenium.support.PageFactory

abstract class BasePage(protected val webDriver: WebDriver) {
    init {
        PageFactory.initElements(webDriver, this)
    }

    abstract fun isLoaded(): Boolean
}