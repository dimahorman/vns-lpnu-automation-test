package com.horman.simpleseleniumtest.pages.lpnu

import com.horman.simpleseleniumtest.pages.BasePage
import com.horman.simpleseleniumtest.manager.WebDriverManager.click
import com.horman.simpleseleniumtest.manager.WebDriverManager.isVisible
import com.horman.simpleseleniumtest.manager.WebDriverManager.sendKeys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy

// https://vns.lpnu.ua/login/index.php
class LpnuLoginPage(webDriver: WebDriver) : BasePage(webDriver) {

    @FindBy(id = "username")
    lateinit var usernameInput: WebElement
        private set

    @FindBy(id = "password")
    lateinit var passwordInput: WebElement
        private set

    @FindBy(id = "loginbtn")
    lateinit var loginButton: WebElement
        private set

    fun enterUsername(username: String): Boolean {
       return webDriver.sendKeys(usernameInput, username)
    }

    fun enterPassword(password: String): Boolean {
        return webDriver.sendKeys(passwordInput, password)
    }

    fun pushLoginButton(): Boolean {
        return webDriver.click(loginButton)
    }

    override fun isLoaded(): Boolean {
        return webDriver.isVisible(usernameInput) && webDriver.isVisible(passwordInput)
    }
}