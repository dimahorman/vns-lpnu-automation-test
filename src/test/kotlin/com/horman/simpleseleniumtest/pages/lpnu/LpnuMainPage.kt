package com.horman.simpleseleniumtest.pages.lpnu

import com.horman.simpleseleniumtest.manager.WebDriverManager.click
import com.horman.simpleseleniumtest.manager.WebDriverManager.isVisible
import com.horman.simpleseleniumtest.pages.BasePage
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.PageFactory

class LpnuMainPage(webDriver: WebDriver) : BasePage(webDriver = webDriver) {

    @FindBy(id = "action-menu-toggle-1")
    lateinit var actionMenu: WebElement
        private set

    @FindBy(css = "a[data-title='profile,moodle']")
    lateinit var dropdownProfileLink: WebElement
        private set

    @FindBy(css = ".notifications .alert-success")
    lateinit var successMessage: WebElement
        private set

    init {
        PageFactory.initElements(webDriver, this)
    }

    fun clickActionMenu(): Boolean {
        return webDriver.click(actionMenu)
    }

    fun clickDropdownProfileLink(): Boolean {
        return webDriver.click(dropdownProfileLink)
    }

    fun isSubmittedChangeSuccessful(): Boolean {
        return webDriver.isVisible(successMessage)
    }

    override fun isLoaded(): Boolean {
        return webDriver.isVisible(actionMenu)
    }
}