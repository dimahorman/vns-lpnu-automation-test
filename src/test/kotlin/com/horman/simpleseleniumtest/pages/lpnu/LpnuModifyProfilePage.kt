package com.horman.simpleseleniumtest.pages.lpnu

import com.horman.simpleseleniumtest.manager.WebDriverManager.click
import com.horman.simpleseleniumtest.manager.WebDriverManager.isVisible
import com.horman.simpleseleniumtest.manager.WebDriverManager.sendKeys
import com.horman.simpleseleniumtest.pages.BasePage
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy

class LpnuModifyProfilePage(webDriver: WebDriver) : BasePage(webDriver) {

    @FindBy(css = ".dndupload-arrow")
    lateinit var uploadArrow: WebElement
        private set

    @FindBy(css = ".fp-thumbnail")
    lateinit var newlyAddedImage: WebElement
        private set

    @FindBy(css = ".fp-upload-btn")
    lateinit var uploadButton: WebElement
        private set

    @FindBy(id = "id_submitbutton")
    lateinit var submitButton: WebElement
        private set

    @FindBy(id = "id_deletepicture")
    lateinit var deletePictureInput: WebElement
        private set

    @FindBy(css = ".px-3 input")
    lateinit var chooseFileButton: WebElement
        private set

    fun clickUploadArrow(): Boolean {
        return webDriver.click(uploadArrow)
    }

    fun toggleDeletePictureInput(): Boolean {
        return webDriver.click(deletePictureInput)
    }

    fun isDeletePictureInputSelected(): Boolean {
        return deletePictureInput.isSelected
    }

    fun isUserPictureSet(): Boolean {
        return webDriver.isVisible(deletePictureInput)
    }

    fun isNewImageAdded(): Boolean {
        return webDriver.isVisible(newlyAddedImage)
    }

    fun clickSubmitButton(): Boolean {
        return webDriver.click(submitButton)
    }

    fun chooseFile(filePath: String): Boolean {
        return webDriver.sendKeys(chooseFileButton, filePath)
    }

    fun uploadFile(): Boolean {
        return webDriver.click(uploadButton)
    }

    override fun isLoaded(): Boolean {
       return webDriver.isVisible(uploadArrow)
    }
}