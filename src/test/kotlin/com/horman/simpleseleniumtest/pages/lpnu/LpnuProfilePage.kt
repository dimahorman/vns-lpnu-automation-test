package com.horman.simpleseleniumtest.pages.lpnu

import com.horman.simpleseleniumtest.manager.WebDriverManager.click
import com.horman.simpleseleniumtest.manager.WebDriverManager.isVisible
import com.horman.simpleseleniumtest.pages.BasePage
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.PageFactory

// https://vns.lpnu.ua/user/profile.php?id=152315
class LpnuProfilePage(driver: WebDriver) : BasePage(driver) {

    @FindBy(id = "action-menu-toggle-2")
    lateinit var optionsButton: WebElement

    @FindBy(css = ".dropdown-item a")
    lateinit var modifyInfoButton: WebElement

    init {
        PageFactory.initElements(driver, this)
    }

    fun clickOptionsButton(): Boolean {
        return webDriver.click(optionsButton)
    }

    fun clickDropdownMenuOption(): Boolean {
        return webDriver.click(modifyInfoButton)
    }

    override fun isLoaded(): Boolean {
        return webDriver.isVisible(optionsButton)
    }
}