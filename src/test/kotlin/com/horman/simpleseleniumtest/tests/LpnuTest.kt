package com.horman.simpleseleniumtest.tests

import com.horman.simpleseleniumtest.manager.LpnuTestManager
import com.horman.simpleseleniumtest.manager.WebDriverManager
import com.horman.simpleseleniumtest.util.WebDriverFactory
import org.junit.jupiter.api.*

class LpnuTest {
    lateinit var testManager: LpnuTestManager

    companion object {
        const val TEST_USERNAME = "XXXXX"
        const val TEST_PASSWORD = "XXXXX"
        const val TEST_FILE_PATH = "src/main/resources/test.gif"
        const val TEST_URL = "https://vns.lpnu.ua"
    }

    @BeforeEach
    fun setUpAll() {
        WebDriverManager.webDriver = WebDriverFactory.getDriver()
        WebDriverManager.webDriver.get(TEST_URL)
        testManager = LpnuTestManager(WebDriverManager.webDriver)
    }

    @AfterEach
    fun close() {
        WebDriverManager.webDriver.close()
    }

    @Test
    fun uploadProfilePhotoTest() {
        testManager.login(TEST_USERNAME, TEST_PASSWORD)
        testManager.goToProfilePage()
        testManager.goToModifyProfilePage()
        testManager.uploadPhoto(TEST_FILE_PATH)
    }
}
