package com.horman.simpleseleniumtest.util

import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.opera.OperaDriver

object WebDriverFactory {
    fun getDriver(name: String = "chrome"): WebDriver {
        return if (name.equals("firefox", ignoreCase = true)) {
            FirefoxDriver()
        } else if (name.equals("opera", ignoreCase = true)) {
            OperaDriver()
        } else {
            ChromeDriver()
        }
    }
}